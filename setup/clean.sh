#!/bin/bash

set -e

cd $(dirname "$BASH_SOURCE")/..

. jenkins-docker.conf
eval $(docker-machine env "${JENKINS_DOCKER_HOST}")

docker-compose down --volumes --rmi all || true
docker-machine rm -y "${JENKINS_DOCKER_HOST}"

rm jenkins-docker.conf
