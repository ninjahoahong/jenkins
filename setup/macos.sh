#!/bin/bash

set -e
CONF_DIR=$(cd $(dirname "$BASH_SOURCE")/..; pwd)

# find out which commands to install
INSTALL=$(
    for COMMAND in docker docker-compose docker-machine
    do
        which "$COMMAND" >/dev/null || echo "$COMMAND"
    done
)

# ensure any missing commands get installed
[ -n "$INSTALL" ] && brew reinstall $INSTALL

# ensure the virtual host to run containers in is always on
brew services list | grep -q 'docker-machine started' ||
    brew services start docker-machine

# ensure we have a virtual host to run containers in
JENKINS_DOCKER_HOST=${1:-docker-host}
docker-machine ls -q | grep "$JENKINS_DOCKER_HOST" | grep -q . ||
    docker-machine create "$JENKINS_DOCKER_HOST"

JENKINS_EXPOSE_PORT=${2:-8080}

# expose Jenkins on localhost
if ! VBoxManage showvminfo "$JENKINS_DOCKER_HOST" --machinereadable | grep -q 'Forwarding.*,8080'
then
    # find out an unreserved port to expose Jenkins at
    while lsof -t -i @127.0.0.1:$JENKINS_EXPOSE_PORT | grep -q .
    do
        JENKINS_EXPOSE_PORT=$[$JENKINS_EXPOSE_PORT + 1]
    done

    if docker-machine ls --filter state=running -q | grep -q "$JENKINS_DOCKER_HOST"
    then
        docker-machine stop "$JENKINS_DOCKER_HOST"
    fi

    NUM_RULES=$(VBoxManage showvminfo docker-host --machinereadable | grep -c ^Forwarding)
    VBoxManage modifyvm "$JENKINS_DOCKER_HOST" --natpf$NUM_RULES \
        jenkins,tcp,127.0.0.1,$JENKINS_EXPOSE_PORT,,8080

    docker-machine start "$JENKINS_DOCKER_HOST"
fi

# write our configuration down for use in start
cat > "$CONF_DIR/jenkins-docker.conf" <<END
JENKINS_DOCKER_HOST=$JENKINS_DOCKER_HOST
JENKINS_EXPOSE_PORT=$JENKINS_EXPOSE_PORT
END
