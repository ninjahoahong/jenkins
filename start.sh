#!/bin/bash

[ "$0" = "$BASH_SOURCE" ] && set -e

cd $(dirname "$BASH_SOURCE")

source jenkins-docker.conf

# ensure our docker host is running
if ! docker-machine ls --filter state=running -q | grep -q "$JENKINS_DOCKER_HOST"
then
    docker-machine start "$JENKINS_DOCKER_HOST"
fi

eval $(docker-machine env "$JENKINS_DOCKER_HOST")

# build the container
docker-compose up -d jenkins

echo "Jenkins available at http://localhost:${JENKINS_EXPOSE_PORT}/" >&2

docker-machine env "$JENKINS_DOCKER_HOST"
