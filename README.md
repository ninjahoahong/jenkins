# automated jenkins setup

## setup

    make setup

or

    ./setup/macos.sh [JENKINS_DOCKER_VIRTUAL_HOST] [PORT_TO_EXPOSE_JENKINS_AT]

### clean

    make clean

or

    ./setup/clean.sh

#### clean all docker machines

Warning! This will drop all docker virtual hosts regardless of their origin.

    make clean-all-docker-machines

## build

    . start.sh

or

    eval $(./start.sh)
