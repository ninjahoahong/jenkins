.PHONY: setup start run clean clean-all-docker-machine

setup: jenkins-docker.conf

jenkins-docker.conf:
	./setup/macos.sh

start: jenkins-docker.conf
	./start.sh

run: start

clean:
	./setup/clean.sh

clean-all-docker-machines:
	docker-machine ls -q | xargs -I {} docker-machine rm -y {}
